<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Registered shop
 */
class Shop extends Model
{

    /**
     * Guarded attributes can't be mass-assigned
     *
     * @var string[]
     */
    protected $guarded = ['id'];

}
