<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Shop;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Controller handling app registration with Shopware
 */
class RegistrationController extends Controller
{
    /**
     * Register app.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        // Validate request.
        // @see https://laravel.com/docs/8.x/validation
        $validator = \Illuminate\Support\Facades\Validator::make(
            $request->all(),
            [
                'shop-id' => 'bail|required',
                'shop-url' => 'bail|required',
            ]
        );
        if ($validator->fails()) {
            return new JsonResponse(['error' => 'Insufficient request parameters'], 400);
        }

        // Check signature.
        $signature = hash_hmac('sha256', urldecode($request->getQueryString()), config('app.shopware_app_secret'));
        if ($request->header('shopware-app-signature') !== $signature) {
            return new JsonResponse(['error' => 'App signature invalid'], 400);
        }

        // Extract request arguments.
        $shopId = $request->get('shop-id');
        $shopUrl = $request->get('shop-url');

        // Get shop by shop id.
        $shop = Shop::where('shop_id', $shopId)->first();
        if (!$shop) {
            $shop = new Shop(
                [
                    'shop_id' => $shopId,
                ]
            );
        }

        // Update and save shop.
        $shop->shop_url = $shopUrl;
        $shop->shop_secret = preg_replace_callback(
            '/\*/u',
            function (): string {
                switch (mt_rand(0, 2)) {
                    // Number
                    case 0:
                        return chr(mt_rand(48, 57));

                    // Uppercase letter
                    case 1:
                        return chr(mt_rand(65, 90));

                    // Lowercase letter
                    case 2:
                        return chr(mt_rand(97, 122));
                }
            },
            str_repeat('*', 128)
        );
        $shop->active = false;
        if (!$shop->save()) {
            return new JsonResponse(['error' => 'Registration failed'], 400);
        }

        // Send final response.
        return new JsonResponse(
            [
                'proof' => hash_hmac(
                    'sha256',
                    $shopId . $shopUrl . config('app.shopware_app_name'),
                    config('app.shopware_app_secret')
                ),
                'secret' => $shop->shop_secret,
                'confirmation_url' => url('/app/confirm'),
            ]
        );
    }

    /**
     * Confirm app registration.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirm(Request $request): JsonResponse
    {
        // Get request body.
        $data = json_decode($request->getContent(), true);

        // Get shop by shop id and url.
        /** @var \App\Models\Shop $shop */
        $shop = Shop::where('shop_id', $data['shopId'])->where('shop_url', $data['shopUrl'])->first();
        if (!$shop) {
            return new JsonResponse(['error' => 'Shop not found'], 400);
        }

        // Check signature.
        $signature = hash_hmac('sha256', $request->getContent(), $shop->shop_secret);
        if ($request->header('shopware-shop-signature') !== $signature) {
            return new JsonResponse(['error' => 'Shop signature invalid'], 400);
        }

        // Save API and secret key.
        $shop->update(
            [
                'api_key' => $data['apiKey'],
                'secret_key' => $data['secretKey'],
            ]
        );

        // Return empty response as sign of success (there's nothing documented in the Shopware docs what to do here).
        return new JsonResponse();
    }
}
