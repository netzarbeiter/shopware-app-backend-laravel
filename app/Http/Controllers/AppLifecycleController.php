<?php

declare(strict_types=1);

namespace App\Http\Controllers;

/**
 * Controller dealing with webhooks for app lifecycle events
 */
class AppLifecycleController
{
    /**
     * Handle the 'app.installed' webhook.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function installed(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        // @todo Add some meaningful code

        return new \Illuminate\Http\JsonResponse();
    }

    /**
     * Handle the 'app.updated' webhook.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updated(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        // @todo Add some meaningful code

        return new \Illuminate\Http\JsonResponse();
    }

    /**
     * Handle the 'app.deleted' webhook.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleted(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        // Delete shop from the database.
        $request->get('shop')->delete();

        return new \Illuminate\Http\JsonResponse();
    }

    /**
     * Handle the 'app.activated' webhook.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activated(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        // Activate shop.
        $request->get('shop')->update(['active' => true]);

        return new \Illuminate\Http\JsonResponse();
    }

    /**
     * Handle the 'app.deactivated' webhook.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deactivated(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        // Activate shop.
        $request->get('shop')->update(['active' => false]);

        return new \Illuminate\Http\JsonResponse();
    }
}
