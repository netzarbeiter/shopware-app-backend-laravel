<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\Shop;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Middleware to validate webhook requests
 */
class ValidateWebhook
{
    /**
     * Handle request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return \App\Http\Middleware\JsonResponse|mixed
     */
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {
        // Get shop by shop id and url.
        /** @var \App\Models\Shop $shop */
        $shop = Shop::where('shop_id', $request->input('source.shopId'))->first();
        if (!$shop) {
            return new JsonResponse(['error' => 'Shop not found'], 400);
        }

        // Check signature.
        $signature = hash_hmac('sha256', $request->getContent(), $shop->shop_secret);
        if ($request->header('shopware-shop-signature') !== $signature) {
            return new JsonResponse(['error' => 'Shop signature invalid'], 400);
        }

        // Extend request with more information to make further processing easier.
        $request = $request->merge(compact('shop'));

        return $next($request);
    }
}
