<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Webhook Routes
|--------------------------------------------------------------------------
|
| Here is where you can register webhook routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "webhook" middleware group. Now create something great!
|
*/

// Register routes for application lifecycle events.
Route::post('/app/installed', [\App\Http\Controllers\AppLifecycleController::class, 'installed']);
Route::post('/app/updated', [\App\Http\Controllers\AppLifecycleController::class, 'updated']);
Route::post('/app/deleted', [\App\Http\Controllers\AppLifecycleController::class, 'deleted']);
Route::post('/app/activated', [\App\Http\Controllers\AppLifecycleController::class, 'activated']);
Route::post('/app/deactivated', [\App\Http\Controllers\AppLifecycleController::class, 'deactivated']);
