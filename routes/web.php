<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Default route.
// @todo Adjust redirect target
Route::get('/', function () {
    return redirect('https://www.my-shopware-company.com');
});

// Routes for app registration with Shopware.
Route::prefix('app')->group(function () {
    Route::get('/register', [\App\Http\Controllers\RegistrationController::class, 'register']);
    Route::post('/confirm', [\App\Http\Controllers\RegistrationController::class, 'confirm']);
});
