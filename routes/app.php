<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
|
| Here is where you can register app routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "app" middleware group. Now create something great!
|
*/

// Routes for app registration with Shopware.
Route::get('/register', [\App\Http\Controllers\RegistrationController::class, 'register']);
Route::post('/confirm', [\App\Http\Controllers\RegistrationController::class, 'confirm']);
