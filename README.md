# Base for a Shopware app backend

This project comprises the minimum for the backend of a Shopware app.

It consists of controllers for the app registration and the handling of an app's lifecycle events.

Example of a Shopware app using the urls:

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://raw.githubusercontent.com/shopware/platform/trunk/src/Core/Framework/App/Manifest/Schema/manifest-1.0.xsd">
    <meta>
        <name>ShopwareAppLaravel</name>
        <label>ShopwareAppLaravel</label>
        <description>ShopwareAppLaravel</description>
        <author>My Shopware Company</author>
        <copyright>(c) My Shopware Company</copyright>
        <version>1.0.0</version>
        <license>MIT</license>
    </meta>
    <setup>
        <registrationUrl>https://my-shopware-app.my-shopware-company.com/app/register</registrationUrl>
        <secret>def0000094c95ac9f552fa007fda474b0f2f1e6439448ff70637a34d17d3a53bb2cfec065e2be25d877143a07b2d34844f0f1564b520366927b168d7a47982becf139430</secret>
    </setup>
    <webhooks>
        <webhook name="app.installed" event="app.installed" url="https://my-shopware-app.my-shopware-company.com/webhook/app/installed" />
        <webhook name="app.updated" event="app.updated" url="https://my-shopware-app.my-shopware-company.com/webhook/app/updated" />
        <webhook name="app.deleted" event="app.deleted" url="https://my-shopware-app.my-shopware-company.com/webhook/app/deleted" />
        <webhook name="app.activated" event="app.activated" url="https://my-shopware-app.my-shopware-company.com/webhook/app/activated" />
        <webhook name="app.deactivated" event="app.deactivated" url="https://my-shopware-app.my-shopware-company.com/webhook/app/deactivated" />
    </webhooks>
</manifest>
```
